//bitbucket.org/jan_mrowiec/zad4/src/master/
#include <iostream>
#include <regex>
#include <vector>
#include <string>
#include <stdio.h>


using namespace std;

void reg(string& s)
{
   regex wzor(R"((\b[^Ee\s]\w*a\w*)|(\b[a]\w*))");
   cout << endl << "Wyniki: " << endl;
       for (sregex_iterator i = sregex_iterator(s.begin(), s.end(), wzor); i != sregex_iterator(); ++i) {
        smatch wynik = *i;
        cout << wynik.str() << endl;
    }
}

void in()
{
    cout << "Wprowadz tekst" << endl;
    cin.ignore();
    string s;
    getline(cin, s);
    reg(s);
}

void file()
{
    char adr[32];
    cout << "Wprowadz adres" << endl;
    cin >> adr;
    string s;
    FILE* f;
    f = fopen(adr, "r");
    char buf[1024];
    while(fgets(buf, 1024, f) != NULL)
        s.append(buf);
    reg(s);
    fclose(f);
}
int main()
{
    cout << "[1]Tekst z standardowego wejscia" << endl;
    cout << "[2]Tekst z pliku" << endl;
    int a;
    cin >> a;
    if(a == 2)
        file();
    else
        in();
    return 0;
}
